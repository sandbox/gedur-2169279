<?php

/**
 * Validate whether an argument is a valid user.
 *
 * This supports either numeric arguments (UID) or strings (username) and
 * converts either one into the user's UID.  This validator also sets the
 * argument's title to the username.
 */
class views_argument_currentuser_validate_user extends views_plugin_argument_validate_user {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['perm_bypass'] = array('default' => FALSE);
    $options['perm'] = array('default' => NULL);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['restrict_roles']['#access'] = FALSE;

    $form['perm_bypass'] = array(
      '#type' => 'checkbox',
      '#title' => t('Granting access for non-matching users with specific permission'),
      '#default_value' => $this->options['perm_bypass'],
    );

    $perms = array();
    $module_info = system_get_info('module');

    // Get list of permissions
    foreach (module_implements('permission') as $module) {
      $permissions = module_invoke($module, 'permission');
      foreach ($permissions as $name => $perm) {
        $perms[$module_info[$module]['name']][$name] = strip_tags($perm['title']);
      }
    }

    ksort($perms);

    $form['perm'] = array(
      '#type' => 'select',
      '#title' => t('Permission'),
      '#description' => t('If no perm are selected, additional access will not be granted.'),
      '#options' => $perms,
      '#default_value' => $this->options['perm'],
      '#dependency' => array(
        'edit-options-validate-options-currentuser-or-perm-perm-bypass' => array(1),
      ),
      '#prefix' => '<div id="edit-options-validate-options-current-current-user-or-perm-wrapper">',
      '#suffix' => '</div>',
    );
  }

  function validate_argument($argument) {
    global $user;

    $type = $this->options['type'];

    // If the argument is an integer and we're accepting the argument as a uid...
    if (is_numeric($argument) && $argument == (int) $argument && $argument > 0
      && ($type == 'uid' || $type == 'either')) {
      // Build the where clause for the argument.
      $where = 'uid = :argument';

      // If the argument represents the current user...
      if ($argument == $user->uid) {
        // Set the account variable to the current user.
        $account = clone $user;
      }
    }
    // Otherwise accept the argument as a user name if specified.
    elseif ($type == 'name' || $type == 'either') {
      // Build the where clause for the argument.
      $where = "name = :argument";

      // If the argument represents the current user...
      if ($argument == $user->name) {
        // Set the account variable to the current user.
        $account = clone $user;
      }
    }

    // If we don't have a where clause, the argument is invalid.
    if (empty($where)) {
      return FALSE;
    }

    // If the argument doesn't represent the current user account...
    if (empty($account)) {
      // Load a pseudo-account object based on the validator's where clause.
      $account = db_query("SELECT uid, name FROM {users} WHERE " . $where, array(':argument' => $argument))->fetchObject();

      // If the account wasn't found, the argument is invalid.
      if (empty($account)) {
        return FALSE;
      }
    }

    // If the current user is not the account specified by the argument...
    if ($user->uid != $account->uid) {
      // And if we're granting access for non-matching users with specific permission
      if (!empty($this->options['perm_bypass']) && !empty($this->options['perm'])) {
        if (!user_access($this->options['perm'])) {
          return FALSE;
        }
      }
      else {
        // Otherwise return FALSE if the role based fallback isn't enabled or no
        // roles are selected.
        return FALSE;
      }
    }

    $this->argument->argument = $account->uid;
    $this->argument->validated_title = check_plain($account->name);

    return TRUE;
  }
}
