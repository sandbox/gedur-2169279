<?php

/**
 * Implements hook_views_plugins
 */
function views_argument_currentuser_views_plugins() {
  return array(
    'argument validator' => array(
      'currentuser_or_perm' => array(
        'title' => t('Current user or perm'),
        'handler' => 'views_argument_currentuser_validate_user',
      ),
    ),
  );
}
